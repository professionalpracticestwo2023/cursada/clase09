Modelo - Evaluación Parcial

Consignas indiscutibles

El alumno debera utilizar un repositorio que le sera brindado por el docente , para ello enviaran un mail a prof.fabian.lopez@gmail.com
Organizarán las tareas
Dentro del tablero tendran que generar las tareas del mismo para solucionar la/s problematicas encontradas y tratadas para su solucion
Las tareas deberan tener una fecha de finalizacion
Las tareas deberan moverse de posicion una vez que se hayan realizado
El examen comienza a las 18hrs del dia jueves ..... y termina el .... a las 12hrs indefectiblemente
Deberan crear una rama develop para realizar el merge , llegado el caso.
Se sugiere crear una rama que este ligada a una tarea del tablero para luego realizar el merge
Podra tener un parcial con un solo archivo o con varios que tienen la misma estructura pero que han tenido que ser divididos, pudiendo utilizar una porcion o un archivo a modo de muestra.
Utilizar el contenedor brindado en clase, que es el se viene utilizando, con el fin de que todos tengamos la misma infraestructura
El trabajo es individual
Deberán generar la estructura necesaria para no tener archivos dispersos
La consultas son bienvenidas, los tiempos de respuesta pueden ver demorados
Todos los archivos de datos deberán guardarse en una carpeta que se llame Datos
Todos los scripts que se generen .php para la realización de la normalización se deberán almacenar en una carpeta Shells
En el raíz solo deberá existir un index.php, con el menu del último punto en el caso de llegar al mismo.
Los scripts de busqueda deberan estar en el la carpeta Controladores
Los scripts de vistas o diseño deberán estar en la carpeta Vistas
Utilizar nombres de scripts, variables que sean descriptivos , en ingles (IMPORTANTE)
Tareas en comun 

(Objetivo aprobar)

Generar las tareas necesarias en el tablero, asignando prioridades, fechas y un detalle. (de minimo se espera dichos valores).
Tomar el archivo que se le haya asignado y realizar la normalización del mismo en la cantidad de N archivos, tablas para dicho fin (rchivos complementarios que sirvan para enlazar dicha información, pensando en una base de datos,utilizar los campos que son necesarios).
Generar los archivos pivotes (perifericos)
. La extensión de todos los archivos de datos será .Dat
Generar el archivo principal con las claves a los archivos pivotes


Al finalizar la normalización se solicita lo siguiente 

(Objetivo Promocionar)

Brindar al usuario un menu en donde pueda seleccionar algun/os filtros para un criterio de busqueda, que luego realice la acción solicita. Una vez realizado esto, se deberá desplegar dicha información en pantalla, en un archivo .pdf o excel de acuerdo a la selección del usuario.


Bonus track (Objetivo superararse a si mismo, no hay limites)

Generar una inteface de usuario en donde se puede acceder con usuario y password, en donde si el usuario existe y valida sus credenciales pueda acceder al menu que se detalla a continuacion:

Si es usuario admin y el password es 123456

Deberá tener la posibilidad de ver en pantalla un menu en donde se puedan realizar todas las acciones desde borrar los archivos que se generan en base al original, para luego correr paso a paso la normalización, verificando en cada paso que sea el correcto y no se haya salteado alguno.

Podrá acceder al menu de consultas

Si es usuario user y el password es 12345678

Solo deberá acceder al menu de las consultas

Si el usuario no existe que brinde la oportunidad de registrarse y activar su usuario sin necesidad de envio de mail, suponiendo que se registra con un mail y password, el mismo sea activado por default.
